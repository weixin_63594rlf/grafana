package org.spring.grafana.trigger.http;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;

@RestController
@RequestMapping("/ab")
public class ABController {
    /// ab -n 10 -c 10 http: //localhost:8091/ab/query_order_info
    @GetMapping("/query_order_info")
    @HystrixCommand(commandProperties = {
            @HystrixProperty(name =
                    "execution.isolation.thread.timeoutInMilliseconds",
                    value = "550")},
            fallbackMethod = "queryOrderInfoError")
    public String queryOrderInfo() throws InterruptedException {
        new CountDownLatch(1).await();
        return HttpStatus.OK.toString();
    }

    private String queryOrderInfoError() throws InterruptedException {
        return "Fallback Hello";
    }

    /// ab -n 10 -c 10 http: //localhost:8091/ab/stream
    @GetMapping("stream")
    @HystrixCommand(commandProperties =
            {@HystrixProperty(name =
                    "execution.isolation.thread.timeoutInMilliseconds",
                    value = "150")},
            fallbackMethod = "stream_error")
    public ResponseBodyEmitter stream(HttpServletResponse response) throws Exception {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setHeader(HttpHeaders.CACHE_CONTROL, CacheControl.noCache().getHeaderValue());

        ResponseBodyEmitter emitter = new ResponseBodyEmitter();
        emitter.send("异步响应");
        new Thread(() -> {
            for (int i = 0; i < 200; i++) {
                try {
                    emitter.send("hi xfg-dev-tech-grafana\r\n" + i);
                    Thread.sleep(250);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            emitter.complete();
        }).start();
        return emitter;
    }

    public ResponseBodyEmitter stream_error(HttpServletResponse response) throws IOException {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setHeader(HttpHeaders.CACHE_CONTROL, CacheControl.noCache().getHeaderValue());

        ResponseBodyEmitter emitter = new ResponseBodyEmitter();
        emitter.send("Err");
        emitter.complete();
        return emitter;
    }
}
